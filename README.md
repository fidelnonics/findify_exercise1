# findify_exercise1

# Implementing toggle, using suggested design.

Some limitations:

* You can onnly use CSS styling, no JavaScript
* For the Yotpo image you can use any image you like 

Design:
* Enabled state: http://prntscr.com/mwsxz2
* Disabled state: http://prntscr.com/mwsyfs

# A running sample can be found [here](https://fidelnonics.gitlab.io/findify_exercise1)